﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NDream.AirConsole;
using UnityEngine;

public class TestD_AirConsoleManager : MonoBehaviour
{
    [SerializeField] private int[] cardIds1; //so they can be shown in unity editor
    [SerializeField] private int[] cardIds2;
    [SerializeField] private int[] cardIds3;
    [SerializeField] private int[] cardIds4;
    // Start is called before the first frame update
    void Awake()
    {
        if (GameObject.FindGameObjectsWithTag("DiscussionPhaseAirConsoleManager").Length >= 2)
        {
            gameObject.SetActive(false);
            return;
        }
        CardsDatabase.Init();
        int[][] cardIds = {cardIds1, cardIds2, cardIds3, cardIds4};
        AirConsole.instance.SetActivePlayers(4);
        Inventory[][] inventories = new Inventory[AirConsole.instance.GetActivePlayerDeviceIds.Count][];
        for (int i = 0; i < inventories.Length; i++)
        {
            Card[] cards = new Card[cardIds[i].Length];
            for (int j = 0; j < cards.Length; j++)
            {
                cards[j] = CardsDatabase.cardList[cardIds[i][j]];
            }
            inventories[i] = new Inventory[] { new Inventory(cards) };
        }
        DiscussionPhaseAirConsoleManager manager = GameObject.FindWithTag("DiscussionPhaseAirConsoleManager").GetComponent<DiscussionPhaseAirConsoleManager>();
        manager.Initialize(inventories);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
