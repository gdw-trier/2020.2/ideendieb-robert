﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json.Utilities;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.UI;

public class Computer : MonoBehaviour
{
    private Interactor lastUser = null;
    private float userActiveTimeLeft = 0f;

    private Card card = null;
    private ComputerManager manager;

    public delegate void VoidNone();
    public VoidNone OnStateChange;

    [SerializeField] private GameObject canv = null;
    [SerializeField] private Image backgroundImage = null;
    [SerializeField] private Sprite computerActive = null;
    [SerializeField] private Sprite computerInactive = null;
    private SpriteRenderer spriteRenderer = null;
    
    private void Awake()
    {
        OnStateChange += ShowCard;
        TryGetComponent(out spriteRenderer);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void FixedUpdate()
    {
        if (lastUser != null)
        {
            userActiveTimeLeft -= Time.deltaTime;
            if (userActiveTimeLeft <= 0f)
            {
                userActiveTimeLeft = 0f;
                lastUser = null;
            }
        }
    }

    public ComputerInteraction Interact(Interactor interactor)
    {
        ComputerInteraction action = null;
        if (lastUser != null)
        {
            action = new HackInventoryAction(interactor, this, lastUser.ComputerInventory, userActiveTimeLeft);
        }
        else if (card != null)
        {
            action = new TakeCardAction(interactor, this, card);
        }
        else
        {
            action = new AccessInventoryAction(interactor, this);
        }
        
        return action;
    }

    public bool CanStealFrom(Inventory inventory)
    {
        return lastUser != null && inventory == lastUser.ComputerInventory;
    }

    public void SetManager(ComputerManager manager)
    {
        this.manager = manager;
    }

    public void SetLastAccessedBy(Interactor interactor, bool stayOnForever)
    {
        lastUser = interactor;
        userActiveTimeLeft = stayOnForever ? Single.PositiveInfinity : manager.HackableTimeAfterAccess;
        OnStateChange?.Invoke();
    }

    public void WasStolenFrom()
    {
        // if (float.IsPositiveInfinity(userActiveTimeLeft)) //still accessing
        // {
        //     return; //without this: user can be stolen from only once while logged in, and once more afterwards
        // }
        lastUser = null;
        userActiveTimeLeft = 0f;
        OnStateChange?.Invoke();
    }

    public bool TrySetCard(Card card)
    {
        if (this.card != null || this.lastUser != null)
        {
            return false;
        }
        this.card = card;
        ShowCard();
        OnStateChange?.Invoke();
        return true;
    }

    public Card TakeCard()
    {
        Card card = this.card;
        this.card = null;
        if (card != null)
        {
            OnStateChange?.Invoke();
        }
        backgroundImage.color = Color.clear;
        return card;
    }

    private void ShowCard()
    {
        if (card == null)
        {
            backgroundImage.color = Color.clear;
            spriteRenderer.sprite = computerInactive;
            return;
        }

        backgroundImage.sprite = GetCardSprite(card);
        backgroundImage.color = Color.white;
        spriteRenderer.sprite = computerActive;
    }

    private Sprite GetCardSprite(Card card)
    {
        string filename = "";
        string chId = card.value.ToString();
        string chTheme = card.theme.ToString();
        filename = "CardsGlow/" + chTheme + chId;
        Sprite toReturn = Resources.Load<Sprite>(filename.Trim());
        return toReturn;
    }
}
