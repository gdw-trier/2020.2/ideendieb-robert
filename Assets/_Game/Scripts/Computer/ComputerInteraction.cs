﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerInteraction
{
    protected Interactor interactor;
    protected Computer computer;
    private bool ending = false;
    public bool Ending => ending;

    public ComputerInteraction(Interactor interactor, Computer computer)
    {
        this.interactor = interactor;
        this.computer = computer;
    }

    public virtual void End()
    {
        ending = true;
        interactor.UnsetCurrentInteraction();
        interactor.ui.CloseComputerInteraction();
        computer.OnStateChange -= this.End;
    }

    public void PlayerLeaveFrom(GameObject gameObject)
    {
        if (gameObject == computer.gameObject)
        {
            this.End();
        }
    }
}

public class TakeCardAction : ComputerInteraction
{
    public TakeCardAction(Interactor interactor, Computer computer, Card card) : base(interactor, computer)
    {
        // Debug.Log("TakeCardAction");
        interactor.ui.DisplayCard(card, this);
        computer.OnStateChange += this.End;
    }

    public void Run(int overwriteSlot = -1)
    {
        if (interactor.MobileInventory.TakeFrom(computer, overwriteSlot))
        {
            this.End();
        }
    }
    
}

public class AccessInventoryAction : ComputerInteraction
{
    public AccessInventoryAction(Interactor interactor, Computer computer) : base(interactor, computer)
    {
        interactor.ui.DisplayComputerInventory(interactor.ComputerInventory, this);
    }

    public void Run(int mobileSlot, int computerSlot)
    {
        if (mobileSlot == -1)
        { mobileSlot = interactor.MobileInventory.GetFreeSlot(); }
        if (computerSlot == -1)
        { computerSlot = interactor.ComputerInventory.GetFreeSlot(); }
        if (mobileSlot == -1 || computerSlot == -1)
        { return; }
        
        computer.SetLastAccessedBy(interactor, true);
        Inventory.Swap(new Inventory.Slot() {inventory = interactor.MobileInventory, index = (uint)mobileSlot},
            new Inventory.Slot() {inventory = interactor.ComputerInventory, index = (uint)computerSlot});
    }

    public override void End()
    {
        computer.SetLastAccessedBy(interactor, false);
        base.End();
    }
}

public class HackInventoryAction : ComputerInteraction
{
    public HackInventoryAction(Interactor interactor, Computer computer, Inventory targetInventory, float timeLeft) : base(interactor, computer)
    {
        interactor.ui.DisplayHackInventory(targetInventory, timeLeft, this);
        computer.OnStateChange += this.End;
    }
    
    public void Run(Inventory.Slot stealSlot)
    {
        if (interactor.MobileInventory.Steal(stealSlot, computer))
        {
            computer.WasStolenFrom();
            End();
        }
    }
}
