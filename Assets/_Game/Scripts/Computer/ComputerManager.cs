﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerManager : MonoBehaviour
{
    [SerializeField] private float cardSpawnDelayMin = 3f;
    [SerializeField] private float cardSpawnDelayMax = 8f;
    [SerializeField] private List<Card> cards;
    
    private Computer[] computers;

    private System.Random rand = new System.Random();
    
    private float nextCardDelay = 0f;
    private float hackableTimeAfterAccess = 5f;
    public float HackableTimeAfterAccess { get => hackableTimeAfterAccess; }
    
    // Start is called before the first frame update
    void Start()
    {
        cards = CardsDatabase.cardList;
        GameObject[] computersObj = GameObject.FindGameObjectsWithTag("Computer");
        computers = new Computer[computersObj.Length];
        for (int i = 0; i < computers.Length; i++)
        {
            computers[i] = computersObj[i].GetComponent<Computer>();
            computers[i].SetManager(this);
        }
        
        nextCardDelay = (float) rand.NextDouble() * (cardSpawnDelayMax - cardSpawnDelayMin) + cardSpawnDelayMin;
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        nextCardDelay -= Time.deltaTime;
        if (nextCardDelay <= 0f)
        {
            Card card = cards[rand.Next(cards.Count - 1)];
            
            List<Computer> uncheckedComputers = new List<Computer>(computers);
            while (uncheckedComputers.Count != 0)
            {
                int iComputer = rand.Next(uncheckedComputers.Count - 1);
                if (uncheckedComputers[iComputer].TrySetCard(card))
                {
                    break;
                }
                else
                {
                    uncheckedComputers.RemoveAt(iComputer);
                }
            }
            
            nextCardDelay = (float) rand.NextDouble() * (cardSpawnDelayMax - cardSpawnDelayMin) + cardSpawnDelayMin;
        }
    }
}
