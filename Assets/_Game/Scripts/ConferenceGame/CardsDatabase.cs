﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardsDatabase : MonoBehaviour
{
   public static List<Card> cardList = new List<Card>();

   private static bool init = false;

   private void Awake()
   {
       Init();
   }

   public static void Init()
    {
        if (init)
        {
            return;
        }

        init = true;
        // public Card(int id, char theme, int value, string crdname, string descr)
        cardList.Add (new Card (0,'b',2,"Undersupplied genre","Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (1,'b',2,"Adjustment of staff went unnoticed", "Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (2,'b',2,"Let's take a risk","Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (3,'b',3,"Economic recession","Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (4,'b',3,"Undersupplied genre","Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (5,'b',3,"Microtransactions could be added","Bonus: Multiply smallest business card value with Nr. of business cards."));
        cardList.Add (new Card (6,'c',1,"Last game was barely buggy","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (7,'c',1,"Community manager","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (8,'c',2,"Popular producer","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (9,'c',2,"The community fixes bugs","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (10,'c',5,"Hype!!","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (11,'c',5,"Offensive topic","Uneven Nr. of customer cards: Sum of customer values is subtracted."));
        cardList.Add (new Card (12,'p',1,"Found a niche talent to exploit","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (13,'p',1,"Good crunch times","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (14,'p',1,"I know a coffee guy","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (15,'p',1,"Striking staff","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (16,'p',2,"Creative freedom","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (17,'p',2,"Coffee for the coffee god!","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (18,'p',3,"Creative freedom","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (19,'p',5,"I know a coffee guy","Bonus: Nr. of production cards is added."));
        cardList.Add (new Card (20,'s',1,"Genre is very popular","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (21,'s',1,"Ingame clan system","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (22,'s',1,"Idiots claim the genre","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (23,'s',1,"Teaching witchcraft to children","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (24,'s',2,"Toxic community","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (25,'s',3,"Study finds link between video game and increase in violence","Bonus: Differences of social card values ard added."));
        cardList.Add (new Card (26,'s',4,"Fans accept preorders","Bonus: Differences of social card values ard added."));


        cardList.Add (new Card (27,'n',5,"Hey, listen now!","Changes type to type of the next played card (except neutral)."));
        cardList.Add (new Card (28,'n',5,"Silence!","Changes type to type of the next played card (except neutral)."));
        cardList.Add (new Card (29,'n',5,"Prepare for trouble and make it double","Changes type to type of the next played card (except neutral)."));


        //cardList.Add (new Card (0,"Undersupplied genre",'b',"Ignore one negative card / (playernr) positive business card",1,2));
        //cardList.Add (new Card (1,"Adjustment of staff went unnoticed",'b', "Ignore one negative card / " + nrOfPlayers + " positive business card",1,+2));
        //cardList.Add (new Card (2,"Uncontrollable Risk",'b',"Ignore one negative card / " + nrOfPlayers + "positive business card",2,-2));
        //cardList.Add (new Card (3,"Economic recession",'b',"Ignore one negative card / " + nrOfPlayers + "positive business card",2,-2));
        //cardList.Add (new Card (4,"Microtransactions could be added",'b',"Ignore one negative card / " + nrOfPlayers + "positive business card",2,1));
        //cardList.Add (new Card (5,"Last game was too buggy",'c',"Value -1 for every customer card played this game",2,1));
        //cardList.Add (new Card (6,"Community manager",'c',"Value +1 for every customer card played this game",2,1));
        //cardList.Add (new Card (7,"Popular producer",'c',"Value +1 for every customer card played this game",2,1));
        //cardList.Add (new Card (8,"Offensive topic",'c',"Value -1 for every customer card played this game",2,1));
        //cardList.Add (new Card (9,"Found a niche talent to exploit",'p',"none",2,1));
        //cardList.Add (new Card (10,"Good crunch times",'p',"none",2,1));
        //cardList.Add (new Card (11,"I know a coffee guy",'p',"none",2,1));
        //cardList.Add (new Card (12,"Striking staff",'p',"none",2,1));
        //cardList.Add (new Card (13,"Creative freedom",'p',"none",2,1));
        //cardList.Add (new Card (14,"Genre is very popular",'s',"none",2,1));
        //cardList.Add (new Card (15,"Ingame clan system",'s',"none",2,1));
        //cardList.Add (new Card (16,"Idiots claim the genre",'s',"none",2,1));
        //cardList.Add (new Card (17,"Teaching witchcraft to children",'s',"none",2,1));
        //cardList.Add (new Card (18,"Hey, listen now!",'n',"none",2,1));
        //cardList.Add (new Card (19,"Silence!",'n',"none",2,1));
        //cardList.Add (new Card (20,"Prepare for trouble and make it double",'n',"none",2,1));
        //cardList.Add (new Card (21,"Toxic community",'s',"none",2,1));
        //cardList.Add (new Card (22,"Study finds link between video game and increase in violence",'s',"none",2,1));
        //cardList.Add (new Card (23,"Fans accept preorders",'s',"none",2,1));

    }
}
