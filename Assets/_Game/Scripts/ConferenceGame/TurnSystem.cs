﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;
using NDream.AirConsole;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class TurnSystem : MonoBehaviour
{
    private class PlayerData
    {
        public Transform cardSlot;
        public List<Card> playedCards = new List<Card>();
        public List<Card> availableCards;
        public Inventory handCards;

    }
    
    private enum TurnSteps
    {
        Initial, ChoseValue, PlayCard, EvaluateCurrentRound, ShowFinalWinner, LoseAll,  Waiting
    }

    [SerializeField] private GameObject[] playerAreas;
    [SerializeField] private AudioClip[] cardSounds;
    private AudioSource audioSource;
    [SerializeField] private CountdownConference timer;
    [SerializeField] private GameObject cardPrefab;
    [SerializeField] private Transform cardSlotsParent;
    private PlayerData[] playerDatas;
    [SerializeField] private Vector3 offset;
    public Color col;
    private int currentPlayerIndex = 0;
    private DiscussionPhaseAirConsoleManager manager = null;

    
    private TurnSteps nextStep = TurnSteps.Waiting;
    private TurnSteps currentRealStep = TurnSteps.Initial;
    [SerializeField] private int cardsToPlay = 6;
    private int currentCardsPlayed = 0;
    [SerializeField] private float choseValueTime = 30f;
    [SerializeField] private float playOneCardTime = 10f;
    [SerializeField] private float evaluationTIme = 5f;
    private bool isWaiting = false;
    private int valueToReach = 21;
    private HashSet<int> looserIndexes = new HashSet<int>();
    [SerializeField] private int gameOverSceneIndex = 4;
    [SerializeField] private LastWinner val;
    private delegate void DoAfterTimer();

    private DoAfterTimer timerAction = null;
    
    public void Initialize(Inventory[] playerInventories)
    {
        playerDatas = new PlayerData[playerInventories.Length];
        for (int i = 0; i < playerDatas.Length; i++)
        {
            playerDatas[i] = new PlayerData();
            playerDatas[i].availableCards = new List<Card>(playerInventories[i].Cards);
            playerDatas[i].availableCards.RemoveAll(card => card == null);
            playerDatas[i].handCards = new Inventory(6);
            playerDatas[i].cardSlot = cardSlotsParent.GetChild(i).GetChild(0);
        }
    }

    public Inventory[] GetHandInventories()
    {
        Inventory[] handCards = new Inventory[playerDatas.Length];
        for (int i = 0; i < handCards.Length; i++)
        {
            handCards[i] = playerDatas[i].handCards;
        }
        return handCards;
    }


    private void Awake()
    {
        TryGetComponent(out audioSource);
    }

    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindWithTag("DiscussionPhaseAirConsoleManager").GetComponent<DiscussionPhaseAirConsoleManager>();
        manager.Activate(this);
        InitRound();
    }

    private void InitRound()
    {
        currentPlayerIndex = Random.Range(0, playerDatas.Length);
        nextStep = TurnSteps.Initial;
        currentRealStep = nextStep;
        UpdateAllUI();
    }
    
    // Update is called once per frame
    void Update()
    {
        switch (nextStep)
        {
            case TurnSteps.Waiting:
                break;
            case TurnSteps.Initial:
                //UpdateAllUI();
                nextStep = TurnSteps.ChoseValue;
                break;
            case TurnSteps.ChoseValue :
                nextStep = TurnSteps.Waiting;
                ValueChoice();
                break;
            case TurnSteps.PlayCard:
                nextStep = TurnSteps.Waiting;
                PlayOneCardRound();
                break;
            case TurnSteps.EvaluateCurrentRound:
                nextStep = TurnSteps.Waiting;
                EvaluateCurrentRound();
                break;
            case TurnSteps.ShowFinalWinner:
                SceneManager.LoadScene(gameOverSceneIndex+1);
                break;
            case TurnSteps.LoseAll:
                SceneManager.LoadScene(gameOverSceneIndex);
                break;
        }
    }

    private void ValueChoice()
    {
        currentRealStep = TurnSteps.ChoseValue;
        //manager.ApplyTargetNumberSelectScreen(currentPlayerIndex);
        Debug.Log("Wähle value");
        timer.OnEnd = delegate
        {
           // manager.ApplyTargetNumberSelectScreen(-1);
            nextStep = TurnSteps.PlayCard;
        };
        timer.StartTimer(choseValueTime);
    }


    private void DrawRandomHands()
    {
        System.Random rand = new System.Random();
        foreach (var playerData in playerDatas)
        {
            Card[] cards = new Card[playerData.handCards.Cards.Length];
            for (int i = 0; i < cards.Length; i++)
            {
                int maxIndex = playerData.availableCards.Count - 1 - i;
                if (maxIndex < 0)
                { break; }
                int selectedCardIndex = rand.Next(maxIndex);
                cards[i] = playerData.availableCards[selectedCardIndex];
                playerData.availableCards[selectedCardIndex] = playerData.availableCards[maxIndex];
                playerData.availableCards[maxIndex] = cards[i];
            }
            playerData.handCards.Set(cards);
        }
    }

    private void PlayOneCardRound()
    {
        DrawRandomHands();
        currentRealStep = TurnSteps.PlayCard;
        Debug.Log("Spiele Karten " + currentCardsPlayed);
        timer.OnEnd = delegate
        { 
            nextStep = TurnSteps.EvaluateCurrentRound;
        };
        timer.StartTimer(playOneCardTime);
    }

    private void PlayCardSound()
    {
        int randomIndex = Random.Range(0, cardSounds.Length - 1);
        audioSource.clip = cardSounds[randomIndex];
    }

    private void EvaluateCurrentRound()
    {
        currentRealStep = TurnSteps.EvaluateCurrentRound;
        int winnerIndex = GetCurrentWinner();
        GetCurrentLooser();
        Debug.Log(looserIndexes.Count);
        
        if (winnerIndex < 0 || looserIndexes.Count == playerDatas.Length)
        {
            nextStep = TurnSteps.LoseAll;
            return;
        }
        timer.OnEnd = delegate
        {
            if (playerDatas.Length - looserIndexes.Count <= 1)
            {
                val.Value = winnerIndex;
                nextStep = TurnSteps.ShowFinalWinner;
            }
            else
            {
                currentPlayerIndex = (currentPlayerIndex + 1) % (playerDatas.Length -1);
                nextStep = TurnSteps.ChoseValue;
                foreach (var playerData in playerDatas)
                {
                    playerData.playedCards.Clear();
                }

                foreach (var index in looserIndexes)
                {
                    playerDatas[index].availableCards.Clear();
                }
                UpdateAllUI();
            }
        };
        timer.StartTimer(evaluationTIme);
    }

    [ContextMenu("Play Test Card")]
    public void PlayTestCard()
    {
        PlayCard(0, 0);
    }
    
    public void PlayCard(int player, int inventorySlot)
    {
        if(currentRealStep != TurnSteps.PlayCard)
            return;
        Inventory inventory = playerDatas[player].handCards;
        if (inventorySlot >= inventory.Cards.Length)
        {
            return;
        }
        Card card = inventory.Cards[inventorySlot];
        if (card == null)
        {
            return;
        }
        inventory.RemoveCard((uint)inventorySlot);
        playerDatas[player].availableCards.Remove(card);
        
        cardPrefab.GetComponent<ThisCard>().thisId = card.id;
        GameObject instance = Instantiate(cardPrefab, playerDatas[player].cardSlot);
        instance.transform.localPosition = offset * playerDatas[player].playedCards.Count;
        playerDatas[player].playedCards.Add(card);
        PlayCardSound();
        UpdateAllUI();
    }

    public int GetTotalCardValue(Card[] cards)
    {
        int value = 0; // resulting total card value
        int pCounter = 0; // number of production cards
        int cCounter = 0, cSum = 0; // number of customer cards and sum of their values
        int bCounter = 0, bMinValue = 10; // number of business cards and the smallest value
        bool nLastCard = false; Card nCard = null; // remember if the last played card is a neutral card, if so remember card
        List<Card> sCards = new List<Card>(); // list of all played social cards

        // count cards per theme and add values
        foreach (var card in cards)
        {
            value += card.value; // add up all values
            switch (card.theme)
            {
                case 's':
                    if (nLastCard)
                    {
                        sCards.Add(nCard);
                        nLastCard = false;
                    }
                    sCards.Add(card);
                    break;

                case 'b':
                    if (nLastCard)
                    {
                        bCounter++;
                        if (nCard.value < bMinValue)
                        {
                            bMinValue = nCard.value;
                        }
                        nLastCard = false;
                    }
                    bCounter++;
                    if (card.value < bMinValue)
                    {
                        bMinValue = card.value;
                    }
                    break;

                case 'c':
                    if (nLastCard)
                    {
                        cCounter++;
                        cSum += nCard.value;
                        nLastCard = false;
                    }
                    cCounter++;
                    cSum += card.value;
                    break;

                case 'p':
                    if (nLastCard)
                    {
                        pCounter++;
                        nLastCard = false;
                    }
                    pCounter++;
                    break;

                case 'n':
                    nLastCard = true;
                    nCard = card;
                    break;

                default:
                    break;
            }
        }

        // Add production bonus: number of production cards
        value += pCounter;

        // Add business bonus: smallest played business value * number of business cards
        if (bCounter > 0)
        {
            value += bMinValue * bCounter;
        }

        // Add social bonus: add up all differences of played social cards
        int diff = 0;
        for (int i = 0; i < sCards.Count - 1; i++)
        {
            diff += Math.Abs(sCards[i].value - sCards[i + 1].value);
        }
        value += diff;

        // Add customer value: alternate adding and subtracting the sum of all played customer cards 
        //                     with every customer card played
        value += (cCounter % 2) == 0 ? cSum : -cSum;

        return value;
    }

    public int GetCurrentWinner()
    {
        int winnerIndex = -1;
        int currentLowestAbs = int.MaxValue;

        for (int i=0; i<playerDatas.Length; i++)
        {
            int playerCurrentBestValue = GetTotalCardValue(playerDatas[i].playedCards.ToArray());
            //TODO Fehler bei negativen Wert?
            int currentAbs = valueToReach - playerCurrentBestValue;
            if (currentAbs < 0)
            {
                looserIndexes.Add(i);
                continue;
            }

            if (currentAbs < currentLowestAbs)
            {
                currentLowestAbs = currentAbs;
                winnerIndex = i;
            }
        }
        return winnerIndex;
    }

    private int GetCurrentLooser()
    {
        HashSet<int> draws = new HashSet<int>();
        Dictionary<int, int> valueId = new Dictionary<int, int>();
        
        for (int i = 0; i < playerDatas.Length; i++)
        {
            int playerBestValue = GetTotalCardValue(playerDatas[i].playedCards.ToArray());
            draws.Add(playerBestValue);
            valueId.Add(i, playerBestValue);
        }
        
        int looserIndex = -1;
        int currentLowestAbs = 0;

        for (int i = 0; i < playerDatas.Length; i++){
            
            if(looserIndexes.Contains(i))
                continue;
        
            int playerBestValue = GetTotalCardValue(playerDatas[i].playedCards.ToArray());
            
            int currentAbs = valueToReach - playerBestValue;
            
            if (currentAbs < 0)
            {
                looserIndexes.Add(i);
                continue;
            }

            if (currentAbs >= currentLowestAbs)
            {
                currentLowestAbs = currentAbs;
                looserIndex = i;
            }
            
        }
        
        for (int i = 0; i < playerDatas.Length; i++)
        {
            if (valueId[i] == currentLowestAbs)
                looserIndexes.Add(i);
        }
        //looserIndexes.UnionWith(draws);
        looserIndexes.Add(looserIndex);
        return looserIndex;
    }
    

    public void SetTarget(int target)
    {
        valueToReach = target;
    }

    private void UpdateSingleUI(GameObject playerArea, float fillAmount, int currentPoints, int goal)
    {
        Image fillImage = playerArea.GetComponent<Image>();
        List<Text> points = new List<Text>();

        foreach (Transform child in playerArea.transform)
        {
            points.Add(child.gameObject.GetComponent<Text>());
        }

        fillImage.fillAmount = fillAmount;

        points[0].text = currentPoints.ToString();
        points[1].text = "/" + goal.ToString();
    }

    private void UpdateAllUI()
    {
        for (int i = 0; i < playerDatas.Length; i++)
        {
            int points = GetTotalCardValue(playerDatas[i].playedCards.ToArray());
            int goal = valueToReach;
            float fillAmount = (float) points / goal;

            UpdateSingleUI(playerAreas[i], fillAmount, points, goal);
        }
    }
}
