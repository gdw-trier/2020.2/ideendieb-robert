﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Card
{
    public int id;
    public string crdname;
    public char theme;
    public string descr;
    public int timesOfUse;
    public int value;

    public Card()
    {

    }

    public Card(int id, char theme, int value, string crdname, string descr)
    {
        this.id = id;
        switch (theme)
        {
            case 's':
            case 'b':
            case 'p':
            case 'c':
            case 'i':
            case 'n':
                this.theme = theme;
                break;
            default:
                throw new System.ArgumentException("Card parameter theme must be s, b, p, c , i or n");
        }
        this.value = value;
        this.crdname = crdname;
        this.descr = descr;
    }

    public static dynamic[] GetObjects(Card[] cards)
    {
        if (cards == null)
        {
            return null;
        }
        dynamic[] objects = new dynamic[cards.Length];
        for (int i = 0; i < objects.Length; i++)
        {
            if (cards[i] == null)
            {
                objects[i] = null;
            }
            else
            {
                objects[i] = new
                {
                    cardId = cards[i].id, crdname = cards[i].crdname, thm = cards[i].theme,
                    descr = cards[i].descr, val = cards[i].value
                };
            }
        }

        return objects;
    }

}