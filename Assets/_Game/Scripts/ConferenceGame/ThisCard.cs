﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ThisCard : MonoBehaviour
{
    public List<Card> listElem = new List<Card>();
    public int thisId;

    public int id;
    public string crdname;
    public char theme;
    public string descr;
    public int value;

    public Text nameText;
    public Text valueText;
    public Text themeText;
    public Text descrText;
    
    public Image backgrn, valueFrame, descrFrame;

    // Start is called before the first frame update
    void Start()
    {
        listElem.Add(CardsDatabase.cardList[thisId]);

        id = listElem[0].id;
        crdname = listElem[0].crdname;
        theme = listElem[0].theme;
        descr = listElem[0].descr;
        value = listElem[0].value;

        nameText.text = ""+crdname;
        valueText.text = ""+value;
        switch (theme)
        {
            case 's':
                themeText.text = "Social";
                backgrn.color = new Color(31/255f,185/255f,47/255f);
                valueFrame.color = new Color(114/255f,221/255f,126/255f);
                descrFrame.color = new Color(210/255f,243/255f,208/255f);
                break;
            case 'b':
                themeText.text = "Business";
                backgrn.color = new Color(214/255f,214/255f,0/255f);
                valueFrame.color = new Color(245/255f,242/255f,164/255f);
                descrFrame.color = new Color(226/255f,222/255f,104/255f);
                break;
            case 'p':
                themeText.text = "Production";
                backgrn.color = new Color(209/255f,34/255f,1/255f);
                valueFrame.color = new Color(231/255f,99/255f,73/255f);
                descrFrame.color = new Color(233/255f,126/255f,105/255f);
                break;
            case 'c':
                themeText.text = "Customer";
                backgrn.color = new Color(60/255f,147/255f,238/255f);
                valueFrame.color = new Color(122/255f,171/255f,224/255f);
                descrFrame.color = new Color(192/255f,222/255f,255/255f);
                break;
            case 'i':
                themeText.text = "Idea";
                break;
            case 'n':
                themeText.text = "Neutral";
                backgrn.color = new Color(214/255f,214/255f,214/255f);
                valueFrame.color = new Color(255/255f,255/255f,255/255f);
                descrFrame.color = new Color(255/255f,255/255f,255/255f);
                break;
            default:
                throw new System.ArgumentException("Card parameter theme must be s, b, p, c , i or n");
        }
        descrText.text = ""+descr;
    }

    // Update is called once per frame
    //void Update()
    //{

    //}
}
