﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowWinner : MonoBehaviour
{
    [SerializeField] private LastWinner val;

    private void Awake()
    {
        GetComponent<Text>().text = "Player " + val.Value;
    }
}
