﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class BackToMenu : MonoBehaviour
{
    private VideoPlayer player;

    private void Awake()
    {
        TryGetComponent(out player);
        player.loopPointReached += LoadMenu;
    }


    private void LoadMenu(VideoPlayer vp)
    {
        SceneManager.LoadScene(0);
    }
}
