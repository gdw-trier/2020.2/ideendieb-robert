﻿using System;
using Newtonsoft.Json.Utilities;
using UnityEngine;

public class PlayerUI
{
    private Interactor _interactor;
    private int playerNumber;
    private CollectionPhaseAirConsoleManager _collectionPhaseAirConsoleManager;
    private Inventory currentOtherInventory;

    public PlayerUI(int playerNumber, CollectionPhaseAirConsoleManager collectionPhaseAirConsoleManager)
    {
        this.playerNumber = playerNumber;
        this._collectionPhaseAirConsoleManager = collectionPhaseAirConsoleManager;
    }
    
    //Inventory views shouldn't change unless updated via callback
    //can call action.End() to abort
    public void DisplayHackInventory(Inventory inventory, float timeLeft, HackInventoryAction action)
    {
        //start hack UI and pass hack action, hack UI calls action.Run with slot to steal from
        _collectionPhaseAirConsoleManager.executeHandlers[playerNumber] += data =>
        {
            uint stolenSlot;
            try
            {
                stolenSlot = (uint) data["info"]["computerSlot"];
            }
            catch (ArgumentNullException e)
            {
                Debug.LogError("executeInv for hacking missing slot");
                return;
            }
            action.Run(new Inventory.Slot() {inventory = inventory, index = stolenSlot});
        };
        _collectionPhaseAirConsoleManager.endHandlers[playerNumber] += action.End;
        _collectionPhaseAirConsoleManager.ShowPhoneUI(CollectionPhaseAirConsoleManager.PhoneUI.hackInventory, playerNumber, inventory.Cards);
        currentOtherInventory = inventory;
        inventory.OnChange += UpdateInventory;
    }

    public void DisplayComputerInventory(Inventory inventory, AccessInventoryAction action)
    {
        //action.Run: swap two slot contents
        _collectionPhaseAirConsoleManager.executeHandlers[playerNumber] += data =>
        {
            int mobileSlot = -1;
            int computerSlot = -1;
            try
            {
                mobileSlot = (int) data["info"]["ownSlot"];
                computerSlot = (int) data["info"]["computerSlot"];
            }
            catch (ArgumentNullException e)
            {
                Debug.LogError("executeInv for inventory access missing slot");
            }
            action.Run(mobileSlot, computerSlot);
        };
        _collectionPhaseAirConsoleManager.endHandlers[playerNumber] += action.End;
        _collectionPhaseAirConsoleManager.ShowPhoneUI(CollectionPhaseAirConsoleManager.PhoneUI.computerInventory, playerNumber, inventory.Cards);
        currentOtherInventory = inventory;
        inventory.OnChange += UpdateInventory;
    }

    public void DisplayCard(Card card, TakeCardAction action)
    {
        // Debug.Log("PlayerUI DisplayCard");
        //action.Run: take Card (optional: specify slot which will be overwritten, otherwise fail when full)
        _collectionPhaseAirConsoleManager.executeHandlers[playerNumber] += data =>
        {
            int mobileSlot = (int) data["info"]["ownSlot"];
            action.Run(mobileSlot);
        };
        _collectionPhaseAirConsoleManager.endHandlers[playerNumber] += action.End;
        _collectionPhaseAirConsoleManager.ShowPhoneUI(CollectionPhaseAirConsoleManager.PhoneUI.card, playerNumber, new Card[] {card});
    }
    
    //will always be called on close, even when closed via action.End()
    public void CloseComputerInteraction() {
        _collectionPhaseAirConsoleManager.ShowPhoneUI(CollectionPhaseAirConsoleManager.PhoneUI.none, playerNumber, null);
        _collectionPhaseAirConsoleManager.ClearMessageHandlers(playerNumber);
        if (currentOtherInventory != null)
        {
            currentOtherInventory.OnChange -= UpdateInventory;
        }
    }

    private void UpdateInventory()
    {
        CollectionPhaseAirConsoleManager.UpdateInventoryView(playerNumber, false, currentOtherInventory);
    }
}