﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CountdownConference : MonoBehaviour
{
    bool active = false;
    float currentTime = 0.0f;
    private float startTime = 0.0f;
    int timeFlatSeconds = 0;

    public Text countdownText;
    public Image countdownBar;

    public delegate void VoidNone();

    public VoidNone OnEnd;

    // Start is called before the first frame update
    void Start()
    {
        timeFlatSeconds = (int)currentTime;
        countdownBar.fillAmount = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            currentTime -= Time.deltaTime;
            timeFlatSeconds = (int)currentTime;

            if (currentTime <= 0.0f)
                currentTime = 0.0f;


            countdownText.text = (timeFlatSeconds / 60) + ":" + (timeFlatSeconds % 60).ToString("00");
            countdownBar.fillAmount = currentTime / startTime; 

            if (countdownBar.fillAmount > 0.5f)
            {
                countdownBar.color = Color.Lerp(Color.yellow, Color.green, (countdownBar.fillAmount - 0.5f) * 2);
            }
            else if (countdownBar.fillAmount > 0.0f)
            {
                countdownBar.color = Color.Lerp(Color.red, Color.yellow, countdownBar.fillAmount * 2);
            }
            else
            {
                active = false;
                OnEnd?.Invoke();
            }
        }
        

    }

    public void StartTimer(float startTime)
    {
        currentTime = startTime;
        this.startTime = startTime;
        timeFlatSeconds = (int)currentTime;
        countdownBar.fillAmount = 1.0f;
        active = true;
    }
}
