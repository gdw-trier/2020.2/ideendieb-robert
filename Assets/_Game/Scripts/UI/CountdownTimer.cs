﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour
{
    float currentTime = 0.0f;
    int timeFlatSeconds = 0;
    Color currentColor = new Color(0,1,0,1);

    public float startTime = 300f; // in seconds
    public Text countdownText;
    public Image countdownBar;

    public delegate void VoidNone();

    public VoidNone OnEnd;

    // Start is called before the first frame update
    void Start()
    {
        currentTime = startTime;
        timeFlatSeconds = (int)currentTime;
        countdownBar.color = currentColor;
        countdownBar.fillAmount = 1.0f;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= Time.deltaTime;
        timeFlatSeconds = (int)currentTime;

        if (currentTime <= 0.0f)
            currentTime = 0.0f;


        countdownText.text = (timeFlatSeconds / 60) + ":" + (timeFlatSeconds % 60).ToString("00") ;
        countdownBar.fillAmount = currentTime / startTime;

        if (countdownBar.fillAmount > 0.5f)
        {
            countdownBar.color = Color.Lerp(Color.yellow, Color.green, (countdownBar.fillAmount - 0.5f) * 2);
        }
        else if (countdownBar.fillAmount > 0.0f)
        {
            countdownBar.color = Color.Lerp(Color.red, Color.yellow, countdownBar.fillAmount * 2);
        }
        else
        {
            OnEnd.Invoke();
        }

    }
}
