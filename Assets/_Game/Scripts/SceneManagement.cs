﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagement : MonoBehaviour
{
    [SerializeField] private int tutorialSceneIndex = 2;
    public void LoadTutorialScene()
    {
        SceneManager.LoadScene(tutorialSceneIndex);
    }
}
