﻿using System;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;

[SelectionBase]
public class MainMenu : MonoBehaviour
{
    [SerializeField] private Button playButton;
    [SerializeField] private bool testDiscussion = false;
    
    public void PlayGame(int from, JToken data)
    {
        if (!(data["action"].ToString().Equals("start") && playButton.interactable))
        { return; }
        int index = SceneManager.GetActiveScene().buildIndex + 1;
        if (testDiscussion)
        {
            index++;
        }
        SceneManager.LoadScene(index);
    }

    public void QuitGame()
    {
        Debug.Log("Quitted Game");
        //UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    private void Start()
    {
        AirConsole.instance.onMessage += PlayGame;
    }

    private void OnDestroy()
    {
        AirConsole.instance.onMessage -= PlayGame;
    }
}
