﻿using System;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textElem;
    private Button button;
    private string playTextTemplate;
    
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        playTextTemplate = String.Copy(textElem.text);
        AirConsole.instance.onConnect += OnConnectOrDisconnect;
        AirConsole.instance.onDisconnect += OnConnectOrDisconnect;
        OnConnectOrDisconnect();
    }

    private void OnDestroy()
    {
        AirConsole.instance.onConnect -= OnConnectOrDisconnect;
        AirConsole.instance.onDisconnect -= OnConnectOrDisconnect;
    }

    private void OnConnectOrDisconnect(int deviceId = 0)
    {
        int playerCount;
        try
        {
            playerCount = AirConsole.instance.GetControllerDeviceIds().Count;
            if (playerCount > 4)
            {
                playerCount = 4;
            }
        }
        catch (AirConsole.NotReadyException)
        {
            playerCount = 0;
        }
        UpdatePlayerCount(playerCount);
    }

    private void UpdatePlayerCount(int playerCount)
    {
        String currentPlayText = playTextTemplate.Replace("$", "" + playerCount);
        // Debug.Log("Setting text to " + currentPlayText);
        textElem.text = currentPlayText;
        button.interactable = playerCount >= 2;
    }
}
