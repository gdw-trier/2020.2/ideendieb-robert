﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CreateIntValue")]
public class LastWinner : ScriptableObject
{
    [SerializeField] private int value;

    public int Value
    {
        get => value;
        set => this.value = value;
    }
}
