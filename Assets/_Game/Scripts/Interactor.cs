﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    [SerializeField] private uint mobileInventorySize, computerInventorySize;

    private Inventory mobileInventory, computerInventory;
    public Inventory MobileInventory => mobileInventory;
    public Inventory ComputerInventory => computerInventory;
    
    private ComputerInteraction currentInteraction = null;
    private List<Collider2D> touchingTriggers = new List<Collider2D>();
    
    public PlayerUI ui;
    private bool init = false;

    private void Start()
    {
        EarlyInit();
    }

    public void EarlyInit()
    {
        if (!init)
        {
            mobileInventory = new Inventory(mobileInventorySize);
            computerInventory = new Inventory(computerInventorySize);
            init = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.isTrigger)
        {
            touchingTriggers.Add(other);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.isTrigger)
        {
            touchingTriggers.Remove(other);
        }
        
        currentInteraction?.PlayerLeaveFrom(other.gameObject);
    }

    public void UnsetCurrentInteraction()
    {
        if (currentInteraction != null && !currentInteraction.Ending)
        { currentInteraction.End(); }
        currentInteraction = null;
    }
    
    [ContextMenu("Interact")]
    public void Interact()
    {
        if (currentInteraction != null)
        {
            UnsetCurrentInteraction();
            return;
        }
        
        foreach (Collider2D collider in touchingTriggers)
        {
            Computer computer = collider.gameObject.GetComponent<Computer>();
            if (computer != null)
            {
                currentInteraction = computer.Interact(this);
                return;
            }
        }
    }
};