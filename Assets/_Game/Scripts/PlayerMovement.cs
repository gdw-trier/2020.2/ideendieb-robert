﻿using System;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using UnityEngine;

public class PlayerMovement : MonoBehaviour, IAttackable
{
    //Make sure to Lock Rotation on Playercharacter in Inspector (unwanted Rotation could result in unexpected/unpleasant behaviour of the controls)!

    private Rigidbody2D rb;
    [SerializeField] private float speedup = 1;
    [SerializeField] private float interactionRadius = 10;
    [SerializeField] private float attackRange = 3f;
    private Vector2 movement = Vector2.zero;
    private Animator anim;
    private bool stunned = false;
    private bool canAttack = true;

    private void Awake()
    {
        TryGetComponent(out rb);
        anim = GetComponentInChildren<Animator>();
        // Debug.Log("Animator: " + anim);
    }

    private void Start()
    {
        
    }

    public void setMovement(Vector2 movementVector)
    {
        movement = movementVector;
    }
    
    //
    // public void OnXChange(float x)
    // {
    //     movement = new Vector2(x, movement.y);
    // }
    //
    // public void OnYChange(float y)
    // {
    //     movement = new Vector2(movement.x, y);
    // }

    void Update()
    {
        if (stunned)
        {
            rb.velocity = Vector3.zero;
            return;
        }

        Vector3 relDirection = (transform.forward * movement.y + transform.right * movement.x).normalized;
        Vector3 resMovement = new Vector3(relDirection.x * speedup, relDirection.z * speedup, 0);
        rb.velocity = resMovement;
        anim.SetFloat("walkingAbs", movement.magnitude);
        // Debug.Log(movement + "\n" + movement.magnitude);
        //Debug.Log(relDirection + "\n" + resMovement + "\n" + rb.velocity);
    }

    public void OnAttack()
    {
        if(!canAttack)
            return;
        GameObject target;
        canAttack = !CheckIfEnemyIsInRange(attackRange, out target);
        //GameObject target = findNearestAttackeble();
        if (target != null && target != gameObject)
        {
           target.GetComponent<IAttackable>().Attacked(gameObject);
           StartCoroutine(WaitAttackCooldown());
        }
        anim.SetTrigger("attackTrigger");
    }
    
    public IEnumerator OnStun(){
        anim.SetTrigger("stunTrigger");
        yield return new WaitForSeconds(2);
        stunned = false;
    }
    
    public IEnumerator WaitAttackCooldown(){
        yield return new WaitForSeconds(5);
        canAttack = true;
    }
    
    private bool CheckIfEnemyIsInRange(float range,  out GameObject attackTarget)
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, range, Vector2.zero);

        if (hit.collider &&  hit.collider.gameObject != gameObject && (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Bot")))
        {
            attackTarget = hit.collider.gameObject;
            return true;
        }

        attackTarget = gameObject;
        return false;
    }



//finding nearest GameObject to attack/interact with
    public GameObject findNearestAttackeble()
    {
        Collider[] collidersInRange = Physics.OverlapSphere(transform.position, interactionRadius);
        //saving the GameObject closest to the Player and its distance
        GameObject nearest = null;
        double shortestDistance = new double();
        //searching for a nearer object
        foreach (Collider collider in collidersInRange)
        {
            GameObject current = collider.gameObject;
            if(current.GetComponent<IAttackable>() == null){break;}
            if (nearest != null)
            {
                double xDif = current.transform.position.x - gameObject.transform.position.x;
                double yDif = current.transform.position.y - gameObject.transform.position.y;
                double distance = Math.Sqrt(xDif * xDif + yDif * yDif);
                if (shortestDistance > distance)
                {
                    shortestDistance = distance;
                    nearest = current;
                }
            }
            //setting current object as nearest to initialize shortestDistance and be able to compare following objects
            //if only one Object is within the radius its the nearest by default
            else
            {
                nearest = collider.gameObject;
                double xDif = current.transform.position.x - gameObject.transform.position.x;
                double yDif = current.transform.position.y - gameObject.transform.position.y;
                shortestDistance =  Math.Sqrt(xDif * xDif + yDif * yDif);
            }
        }

        return nearest;
    }

    [ContextMenu("IAttackable")]
    public void Attacked(GameObject sender)
    {
        stunned = true;
        StartCoroutine(OnStun());
    }
}
