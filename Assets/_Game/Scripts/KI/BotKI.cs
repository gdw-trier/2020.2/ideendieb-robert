﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BotKI : MonoBehaviour, IAttackable
{
    private Pathfinding pathfindingHandle;
    private Vector3 target;
    private Queue<Vector3> path = new Queue<Vector3>();
    [SerializeField] private float lookForward = 0.5f;
    private Vector3 direction;
    [SerializeField] private float speed = 2f;
    private List<Vector3> pcTargets = new List<Vector3>();
    private int lastRandomIndex = -1;
    //private Rigidbody2D rb = null;
    private GameObject attackTarget = null;
    [SerializeField] private Animator anim;

    private bool isWaiting = false;
    private bool canAttack = true;
    private bool isWalking = false;
    [SerializeField] private int attackChance = 5;
    [SerializeField] private float rangeForPlayerCheck = 1f;
    [SerializeField] private float rangeForPcFree = 1f;
    [SerializeField] private float stunDuration = 2f;
    [SerializeField] private float attackCooldown = 4f;
    
    private enum BotState
    {
        IDLE, SEARCHPC, INTERACTPC, ATTACKPLAYER, ATTACKED
    }

    private BotState state = BotState.IDLE;

    private void Awake()
    {
        //TryGetComponent(out rb);
    }

    void Start()
    {
        pathfindingHandle = Pathfinding.Instance;
        foreach (var pc in GameObject.FindGameObjectsWithTag("Computer"))
        {
            pcTargets.Add(pc.transform.position + Vector3.down);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        float walkingValue = isWalking ? 1f : 0.0f;
        anim.SetFloat("walkingAbs" , walkingValue);
        
        switch (state)
        {
            case BotState.ATTACKED:
                break;
            case BotState.IDLE:
                isWalking = false;
                ChoosePc();
                state = BotState.SEARCHPC;
                break;
            case BotState.SEARCHPC:
                Movement();
                if (CheckIfEnemyIsInRange(rangeForPlayerCheck, out attackTarget) && CheckForPlayerAttack())
                {
                    state = BotState.ATTACKPLAYER;
                }
                if (CheckIfPcIsInRange(0.75f))
                    if (CheckIfPcIsFree())
                    {
                        if (!isWaiting)
                        {
                            isWaiting = true;
                            StartCoroutine(IsWaitingInteracting());
                        }
                    }
                    else
                    {
                        state = CheckForPlayerAttack() ? BotState.ATTACKPLAYER : BotState.IDLE;
                    }

                break;
            case BotState.INTERACTPC:
                isWalking = false;
                InteractPc();
                state = BotState.IDLE;
                break;
            case BotState.ATTACKPLAYER:
                if(canAttack)
                    AttackPlayer();
                state = BotState.IDLE;
                break;
            default:
                state = BotState.IDLE;
                break;
        }
    }

    private void Movement()
    {
        if(path.Count <1 || path == null)
            return;
        
        direction = path.Peek() - transform.position;
        if (direction.magnitude <= lookForward)
            path.Dequeue();

        transform.Translate(direction.normalized * (speed * Time.deltaTime));
        isWalking = true;
        //rb.MovePosition(rb.position + (Vector2)(direction.normalized * (speed * Time.deltaTime)));
    }

    private void ChoosePc()
    {
        int pcCount = pcTargets.Count;
        if(pcCount == 0)
            return;
        int randomIndex = Random.Range(0, pcCount);
        if (randomIndex == lastRandomIndex)
            randomIndex = Mathf.Abs((randomIndex - pcCount + 1) % pcCount);
        lastRandomIndex = randomIndex;
        
        target = pcTargets[randomIndex];
        path.Clear();
        foreach (var node in  pathfindingHandle.FindPath(transform.position, target))
        {
            path.Enqueue(node);
        }
    }

    private void InteractPc()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.up, 1f);
        isWalking = false;
        if (hit.collider.CompareTag("Computer"))
        {
            Computer comp;
            hit.collider.gameObject.TryGetComponent(out comp);
            comp.TakeCard();
        }
    }

    private void AttackPlayer()
    {
        canAttack = false;
        // Debug.Log("Attack");
        if (attackTarget != null && attackTarget != gameObject)
        {
            attackTarget.TryGetComponent<IAttackable>(out var attacked);
            attacked.Attacked(gameObject);
        }
        anim.SetTrigger("attackTrigger");
        StartCoroutine(IsWaitingAttack());
    }

    private bool CheckIfPcIsInRange(float range)
    {
        return (target - transform.position).magnitude <= range;
    }

    private bool CheckIfEnemyIsInRange(float range,  out GameObject attackTarget)
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, range, Vector2.zero);

        if (hit.collider &&  hit.collider.gameObject != gameObject && (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Bot")))
        {
            attackTarget = hit.collider.gameObject;
            return true;
        }

        attackTarget = gameObject;
        return false;
    }
    

    private bool CheckIfPcIsFree()
    {
        RaycastHit2D hit = Physics2D.CircleCast(transform.position, rangeForPcFree, Vector2.zero);
        isWalking = false;
        if (hit.collider && (hit.collider.CompareTag("Player") || hit.collider.CompareTag("Bot")) && hit.collider.gameObject != gameObject)
            return false;
        return true;
    }
    
    private bool CheckForPlayerAttack()
    {
        int randomNum = Random.Range(0, 10);

        return canAttack && randomNum > attackChance;
    }

    private IEnumerator IsWaitingInteracting()
    {
        float randomValue = Random.Range(0, 2f);
        yield return new  WaitForSeconds(2 + randomValue);
        state = BotState.INTERACTPC;
        isWaiting = false;
    }
    
    private IEnumerator IsWaitingAttack()
    {
        yield return new  WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    public void Attacked(GameObject sender)
    {
        state = BotState.ATTACKED;
        StartCoroutine(DisableMovement());
    }

    private IEnumerator DisableMovement()
    {
        anim.SetTrigger("stunTrigger");
        isWalking = false;
        yield return new WaitForSeconds(stunDuration);
        state = BotState.IDLE;
    }
}
