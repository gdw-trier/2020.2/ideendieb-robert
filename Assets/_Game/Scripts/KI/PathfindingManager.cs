﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Scripting;

public class PathfindingManager : MonoBehaviour
{
    [SerializeField] private int width = 4;
    [SerializeField] private int height = 4;
    [SerializeField] private float cellsize = 1f;
    [SerializeField] private Vector3 originPosition = new Vector3(-3, -3, 0);
    [SerializeField] private LayerMask layerToBlock;
    private Pathfinding pathfindingHandle;

    private void Awake()
    {
        //pathfindingHandle = new Pathfinding(width, height, cellsize, originPosition, DetectWalls);
        pathfindingHandle = Pathfinding.Instance;
        pathfindingHandle.InitPathfindingGrid(width, height, cellsize, originPosition, DetectWalls);
    }

    private bool DetectWalls(int x , int y)
    {
        Vector3 pos = GetWorldPosition(x,y);
        RaycastHit2D hit = Physics2D.Raycast(pos, Vector2.zero, layerToBlock);
        return hit.collider == null || hit.collider.isTrigger;
    }
    
    public Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x,y) * cellsize + originPosition +  Vector3.one * (cellsize * 0.5f);
    }
    
}
