﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridTester : MonoBehaviour
{
    private Camera mainCam;
    private Pathfinding grid = null;
    private int currentPathIndex = 0;
    private List<Vector3> pathVectorList = new List<Vector3>();
    void Awake()
    {
        mainCam = Camera.main;
        // = new Pathfinding(4, 4, 1f, new Vector3(-3, -3,0), null);
    }
    

    private void SetTargetPos(Vector3 target)
    {
        currentPathIndex = 0;
        pathVectorList = grid.FindPath(transform.position, target);

        for (int i = 0; i < pathVectorList.Count; i++)
        {
            Debug.Log(pathVectorList[i]);
        }
        
        if (pathVectorList != null && pathVectorList.Count > 1)
        {
            pathVectorList.RemoveAt(0);
        }
    }

}
