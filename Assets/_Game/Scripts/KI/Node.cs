﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Node 
{
    private int x;
    private int y;

    private int gCost;
    private int hCost;
    private int fCost;

    private bool isWalkable = true;
    private Node cameFrom;
    private List<Node> neighborNodes = new List<Node>();

    public int X => x;

    public int Y => y;

    public int GCost
    {
        get => gCost;
        set => gCost = value;
    }
    
    public int HCost
    {
        get => hCost;
        set => hCost = value;
    }

    public bool IsWalkable
    {
        get => isWalkable;
        set => isWalkable = value;
    }

    public Node CameFrom
    {
        get => cameFrom;
        set => cameFrom = value;
    }
    
    public int FCost => fCost;

    public List<Node> NeighborNodes => neighborNodes;

    public Node()
    {
        
    }
    
    public Node( int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public Node(Vector2Int pos)
    {
        x = pos.x;
        y = pos.y;
    }

    public void SetXandY(int nx, int ny)
    {
        x = nx;
        y = ny;
    }

    public void CalculateFCost()
    {
        fCost = gCost + hCost;
    }

    public void CalculateNeighbors(Grid<Node> grid)
    {
        int[] up = {0, 1};
        int[] down = {0, -1};
        int[] right = {1, 0};
        int[] left = {-1, 0};
        int[] leftUp = {-1, 1};
        int[] rightUp = {1, 1};
        int[] leftDown = {-1, -1};
        int[] rightDown = {1, -1};

        int[][] eightSide = {up, down, right, left, leftUp, rightUp, leftDown, rightDown};

        for (int i = 0; i < eightSide.GetLength(0); i++)
        {
            int xCheck = x + eightSide[i][0];
            int yCheck = y + eightSide[i][1];
            
            
            if(xCheck >= 0 && yCheck >= 0 && xCheck < grid.Width && yCheck < grid.Height)
                neighborNodes.Add(grid.GetCellObject(xCheck,yCheck));
        }
        
    }
}
