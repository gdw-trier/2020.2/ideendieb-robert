﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid<TCellType> where TCellType : new()
{
    private int width = 0;
    private int height = 0;
    private float cellsize = 32;
    private TCellType[,] gridArray;
    private Vector3 originPosition = Vector3.zero;

    public int Width => width;

    public int Height => height;

    public float Cellsize => cellsize;

    public Grid(int width, int height, float cellsize)
    {
        this.width = width;
        this.height = height;
        this.cellsize = cellsize;
        gridArray = new TCellType[width,height];
    }
    
    public Grid(int width, int height, float cellsize, Vector3 originPosition)
    {
        this.width = width;
        this.height = height;
        this.cellsize = cellsize;
        this.originPosition = originPosition;
        gridArray = new TCellType[width,height];
    }

    public void DebugGrid()
    {
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x,y+1), Color.green, 100f);
                Debug.DrawLine(GetWorldPosition(x, y), GetWorldPosition(x+1,y), Color.green, 100f);
            }
            
        }
        Debug.DrawLine(GetWorldPosition(0, height), GetWorldPosition(width,height), Color.green, 100f);
        Debug.DrawLine(GetWorldPosition(width, 0), GetWorldPosition(width,height), Color.green, 100f);
    }

    public void InitGrid()
    {
        for (int x = 0; x < gridArray.GetLength(0); x++)
        {
            for (int y = 0; y < gridArray.GetLength(1); y++)
            {
                gridArray[x, y] = new TCellType();
            }
            
        }
    }
    

    public Vector3 GetWorldPosition(int x, int y)
    {
        return new Vector3(x,y) * cellsize + originPosition;
    }

    public (int x, int y) GetXYFromWorldPos(Vector3 worldPosition)
    {
        int x, y = 0;
        x = Mathf.FloorToInt((worldPosition - originPosition).x / cellsize);
        y = Mathf.FloorToInt(((worldPosition - originPosition).y / cellsize));
        return (x,y);
    }
    
    private bool IsLegalWidthHeigth(int x, int y)
    {
        return ((x >= 0) && (x < width) && (y >= 0) && (y < height));
    }

    /// <summary>
    /// Set The Cell Value at Cell[x,y]
    /// </summary>
    /// <param name="x">X value greater equal zero and less than width</param>
    /// <param name="y">> value greater equal zero and less than heigth</param>
    /// <param name="value">CellValue</param>
    public void SetCellObject(int x, int y, TCellType value)
    {
        if(IsLegalWidthHeigth(x,y))
            gridArray[x, y] = value;
    }

    /// <summary>
    /// Set The Cell Value at worldPosition
    /// </summary>
    /// <param name="worldPosition">Worldposition</param>
    /// <param name="value">CellValue</param>
    public void SetCellObject(Vector3 worldPosition, TCellType value)
    {
        var (x, y) = GetXYFromWorldPos(worldPosition);
        SetCellObject(x,y, value);
    }

    public TCellType GetCellObject(int x, int y)
    {
        if (IsLegalWidthHeigth(x,y))
        {
            return gridArray[x, y];
        }

        return default(TCellType);
    }

    public TCellType GetCellObject(Vector3 worldPosition)
    {
        var (x, y) = GetXYFromWorldPos(worldPosition);
        return GetCellObject(x, y);
    }
    
}
