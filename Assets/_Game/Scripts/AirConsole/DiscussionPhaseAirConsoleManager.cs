﻿using System;
using System.Collections;
using System.Collections.Generic;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using UnityEngine;

public class DiscussionPhaseAirConsoleManager : MonoBehaviour
{
    private Inventory[] inventories;
    private TurnSystem turnSystem;

    public void Initialize(Inventory[][] multiInventories)
    {
        this.inventories = new Inventory[multiInventories.Length];
        for (int iMultiInv = 0; iMultiInv < multiInventories.Length; iMultiInv++)
        {
            this.inventories[iMultiInv] = multiInventories[iMultiInv][0];
            for (int j = 1; j < multiInventories[iMultiInv].Length; j++)
            {
                this.inventories[iMultiInv] += multiInventories[iMultiInv][j];
            }
        }
        transform.SetParent(null);
        gameObject.SetActive(true);
        DontDestroyOnLoad(this);
    }

    public void Activate(TurnSystem turnSystem)
    {
        this.turnSystem = turnSystem;
        gameObject.SetActive(true);
        turnSystem.Initialize(inventories);

        var deviceIds = AirConsole.instance.GetActivePlayerDeviceIds;
        if (deviceIds.Count != inventories.Length)
        {
            Debug.LogError("device number (" + deviceIds.Count + ") doesn't match number of passed inventories (" + inventories.Length + ")");
        }

        inventories = turnSystem.GetHandInventories();

        for (int i = 0; i < deviceIds.Count; i++)
        {
            int lambdaIndex = i; //copy for lambda
            Inventory lambdaInventory = inventories[i];
            inventories[i].OnChange += () =>
            {
                UpdateInventoryView(lambdaIndex, lambdaInventory);
            };
            inventories[i].OnChange.Invoke();
        }

        AirConsole.instance.onMessage += OnMessage;
    }

    //show target number selection screen for specified player, inventory screen for all others
    //call with -1 to show inventory screen for all
    public void ApplyTargetNumberSelectScreen(int player)
    {
        var deviceIds = AirConsole.instance.GetActivePlayerDeviceIds;
        for (int i = 0; i < deviceIds.Count; i++)
        {
            if (i == player)
            {
                AirConsole.instance.Message(deviceIds[i], new { action = "targetNumberSelection" });
            }
            else
            {
                UpdateInventoryView(i, inventories[i]);
            }
        }
    }
    
    private void UpdateInventoryView(int playerNumber, Inventory inventory)
    {
        int deviceId = AirConsole.instance.ConvertPlayerNumberToDeviceId(playerNumber);
        string actionStr = "phaseTwo";
        var cards = Card.GetObjects(inventory.Cards);
        AirConsole.instance.Message(deviceId, new { action = actionStr, info = cards });
    }

    void OnMessage(int fromDeviceId, JToken data)
    {
        String msg = data["action"].ToString();
        int player = AirConsole.instance.ConvertDeviceIdToPlayerNumber(fromDeviceId);
        switch (msg)
        {
            case "playCard":
                turnSystem.PlayCard(player, (int)data["info"]["index"]);
                break;
            case "selectTarget":
                turnSystem.SetTarget((int) data["info"]["target"]);
                break;
                
            default:
                //Shit happened (illegal or unexpected message)
                Debug.LogError("Error on Input/Input-Message from " + fromDeviceId + ", Unknown Action: " + data["action"]);
                break;
        }
    }
}
