﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using NDream.AirConsole;
using Newtonsoft.Json.Linq;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class CollectionPhaseAirConsoleManager : MonoBehaviour
{
    //Player Objects
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private DiscussionPhaseAirConsoleManager discussionPhaseAirConsoleManager;
    private GameObject[] playerObjects;
    //PlayerIds; Indices matching those from playerObj

    public delegate void ExecuteHandler(JToken data);
    public ExecuteHandler[] executeHandlers = new ExecuteHandler[4];
    public delegate void EndHandler();
    public EndHandler[] endHandlers = new EndHandler[4];
    private int[] playerIds = new int[4];
    //number of registered players
    private int playerNr = 0;
    /*prevents Player from starting the game more than once,
    which would allow him to spawn more playercharacters than players 
    potentially causing a index out of boundaries exception*/
    private bool started = false;

    void Awake()
    {
        GameObject.FindWithTag("Timer").GetComponent<CountdownTimer>().OnEnd += GoToDiscussionPhase;
        //DontDestroyOnLoad(this);
        AirConsole.instance.onMessage += OnMessage;
        //Setting all Playerobjects (charactest) inactive
    }

    void Start()
    {
        gameStart();
    }

    private void GoToDiscussionPhase()
    {
        Inventory[][] playerInventories = new Inventory[playerObjects.Length][];
        for (int iPlayer = 0; iPlayer < playerInventories.Length; iPlayer++)
        {
            Interactor interactor = playerObjects[iPlayer].GetComponent<Interactor>();
            playerInventories[iPlayer] = new Inventory[]{interactor.MobileInventory, interactor.ComputerInventory};
        }
        discussionPhaseAirConsoleManager.Initialize(playerInventories);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
    //will be called when Game is started over the MainMenu (avoiding timing issues with default start-method
    public void gameStart()
    {
        started = true;
        //Registering the Device-Id of all Connected Controllers(Phones) and setting referenced Playerobjects(Character) active
        //assign playernr to all active devices
        AirConsole.instance.SetActivePlayers(4);
        //get deviceId of all active players and assign to playerObject (Character) in for-loop (up to 4 Players, rest will be ignored) 
        ReadOnlyCollection<int> ids = AirConsole.instance.GetActivePlayerDeviceIds;
        // Debug.Log(ids.Count + " players");
        playerObjects = new GameObject[ids.Count];
        //Debug.Log(ids.Count);
        for(int i = 0; i < ids.Count; i++)
        {
            int id = ids[i];
            playerIds[playerNr] = id;
            playerObjects[playerNr] = GameObject.Instantiate(playerPrefab);
            Interactor interactor = playerObjects[playerNr].GetComponent<Interactor>();
            interactor.EarlyInit();
            interactor.ui = new PlayerUI(playerNr, this);
            int playerNumberCopy = playerNr;
            interactor.MobileInventory.OnChange += () => UpdateInventoryView(playerNumberCopy, true, interactor.MobileInventory);
            //Debug.Log(playerNr + "\n" + playerObjects[playerNr].activeSelf + "\n" + playerIds[playerNr].ToString());
            playerNr++;
        }
        started = true;
    }

    // void register(int deviceId)
    // {
    //     //Debug.Log("OnConnect: " + deviceId.ToString());
    //     playerIds[playerNr] = deviceId;
    //     playerNr++;
    //     Debug.Log(playerNr + "HAS CONNECTED");
    // }

    
    //Process Button inputs (identify and call corresponding method)
    void OnMessage(int fromDeviceId, JToken data)
    {
        //Debug.Log("message from: " + fromDeviceId + ": \ndata: " + data);
        String msg = data["action"].ToString();
        JToken info = data["info"];
        int playerNumber = AirConsole.instance.ConvertDeviceIdToPlayerNumber(fromDeviceId);
        switch (msg)
        {
            case "menu":
                if (!started)
                {
                    gameStart();
                    Debug.Log(started);
                }
                break;
            case "attack":
                OnAttack(fromDeviceId);
                break;
            case "interact":
                OnInteract(fromDeviceId);
                break;
            // case "moveup":
            //     OnMoveV(1, fromDeviceId);
            //     break;
            // case "movedown":
            //     OnMoveV(-1, fromDeviceId);
            //     break;
            // case "moveleft":
            //     OnMoveH(-1, fromDeviceId);
            //     break;
            // case "moveright":
            //     OnMoveH(1, fromDeviceId);
            //     break;
            // case "stopH":
            //     OnMoveH(0, fromDeviceId);
            //     break;
            // case "stopV":
            //     OnMoveV(0, fromDeviceId);
            //     break;
            // case("moveur"):
            //     OnMoveV(0.7f, fromDeviceId);
            //     OnMoveH(0.7f, fromDeviceId);
            //     break;
            // case("moveul"):
            //     OnMoveV(0.7f, fromDeviceId);
            //     OnMoveH(-0.7f ,fromDeviceId);
            //     break;
            // case("movedr"):
            //     OnMoveV(-0.7f, fromDeviceId);
            //     OnMoveH(0.7f, fromDeviceId);
            //     break;
            // case("movedl"):
            //     OnMoveV(-0.7f, fromDeviceId);
            //     OnMoveH(-0.7f, fromDeviceId);
            //     break;
            // case("diagstop") :
            //     OnMoveV(0, fromDeviceId);
            //     OnMoveH(0, fromDeviceId);
            //     break;
            case "move":
                processMovementInput(info, fromDeviceId);
                break;
            case "stopMovement":
                getPlayerObject(fromDeviceId).GetComponent<PlayerMovement>().setMovement(Vector2.zero);
                break;
            case "executeInv":
                executeHandlers[playerNumber]?.Invoke(data);
                Debug.Log("Sending " + data["action"] + " action to player " + playerNumber + " message handlers (Count: " + (executeHandlers[playerNumber] == null ? 0 : executeHandlers[playerNumber].GetInvocationList().Length) + ")");
                break;
            case "endInv":
                endHandlers[playerNumber]?.Invoke();
                Debug.Log("Sending " + data["action"] + " action to player " + playerNumber + " message handlers (Count: " + (endHandlers[playerNumber] == null ? 0 : endHandlers[playerNumber].GetInvocationList().Length) + ")");
                break;
            default:
                //Shit happened (illegal or unexpected message)
                Debug.LogError("Error on Input/Input-Message from " + fromDeviceId + ", Unknown Action: " + data["action"]);
                break;
        }
    }

    public enum PhoneUI
    {
        hackInventory,
        computerInventory,
        card,
        none
    };
    
    public void ShowPhoneUI(PhoneUI ui, int playerNumber, Card[] currentCards)
    {
        int deviceId = AirConsole.instance.ConvertPlayerNumberToDeviceId(playerNumber);
        // Debug.Log(playerNumber);
        var currentCardsObj = Card.GetObjects(currentCards);
        // Debug.Log("CollectionPhaseManagerShowPhoneUI");
        switch (ui)
        {
        case PhoneUI.hackInventory:
            // Debug.Log("Case HACK");
            AirConsole.instance.Message(deviceId, new { action = "displayHackInventory", cards = currentCardsObj });
            break;
        case PhoneUI.computerInventory:
            // Debug.Log("Case COMPUTER");
            AirConsole.instance.Message(deviceId, new { action = "displayComputerInventory", cards = currentCardsObj });
            break;
        case PhoneUI.card:
            // Debug.Log("Case CARD");
            AirConsole.instance.Message(deviceId, new { action = "displayCard", cards = currentCardsObj });
            break;
        case PhoneUI.none:
            // Debug.Log("Case NONE");
            AirConsole.instance.Message(deviceId, new { action = "closeDisplay" });
            break;
        }
    }

    private void processMovementInput(JToken info, int deviceId)
    {
        float x = info["x"].ToObject<float>();
        float y = info["y"].ToObject<float>();
        Vector2 movementVector = new Vector2(x, y);
        getPlayerObject(deviceId).GetComponent<PlayerMovement>().setMovement(movementVector);
    }


    private GameObject getPlayerObject(int deviceId)
    {
        GameObject currentPlayer = null;
        //searching for Player(Game)Object matching the given deviceID
        for (int i = 0; i < playerNr; i++)
        {
            if (playerIds[i] == deviceId)
            {
                currentPlayer = playerObjects[i];
                // Debug.Log("Current Player found: " + deviceId + "\t" + i);
                break;
            }
        }

        return currentPlayer;
    }
    
    private void OnInteract(int deviceId)
    {
        getPlayerObject(deviceId).GetComponent<Interactor>().Interact();
    }
    
    private void OnAttack(int deviceId)
    {
       getPlayerObject(deviceId).GetComponent<PlayerMovement>().OnAttack();

    }

    //Handling vertical movement
    // private void OnMoveV(float input, int deviceId)
    // {
    //     GameObject currentPlayer = getPlayerObject(deviceId);
    //     //Checking if player was found and calling method in related Playermovement script
    //     if (currentPlayer != null)
    //     {
    //         currentPlayer.GetComponent<PlayerMovement>().OnYChange(input);
    //     }
    // }
    //
    // //Handling horizontal movement
    // private void OnMoveH(float input, int deviceId)
    // {
    //     //searching for Player(Game)Object matching the given deviceID
    //     GameObject currentPlayer = getPlayerObject(deviceId);
    //     if (currentPlayer != null)
    //     {
    //         currentPlayer.GetComponent<PlayerMovement>().OnXChange(input);
    //     }
    // }
    //
    //OnDestroy Method as given by (the used) AirConsole reference-Script
    private void OnDestroy()
    {
        // Debug.Log("OnDestroy");
        if (AirConsole.instance != null)
        {
            AirConsole.instance.onMessage -= OnMessage;
        }
    }

    public void ClearMessageHandlers(int playerNumber)
    {
        executeHandlers[playerNumber] = null;
        endHandlers[playerNumber] = null;
    }

    public static void UpdateInventoryView(int playerNumber, bool mobileInventory, Inventory inventory)
    {
        int deviceId = AirConsole.instance.ConvertPlayerNumberToDeviceId(playerNumber);
        string actionStr = mobileInventory ? "updateOwnInventory" : "updateComputerInventory";
        var cards = Card.GetObjects(inventory.Cards);
        AirConsole.instance.Message(deviceId, new { action = actionStr, info = cards });
    }
}
