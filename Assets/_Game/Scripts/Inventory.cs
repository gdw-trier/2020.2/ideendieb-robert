﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory
{
    public struct Slot
    {
        public Inventory inventory;
        public uint index;
    }

    public delegate void VoidNone();

    public VoidNone OnChange;

    // public VoidNone OnChange
    // {
    //     get
    //     {
    //         Debug.Log("get: OnChange has " + onChange?.GetInvocationList().Length);
    //         return onChange;
    //     }
    //     set
    //     {
    //         Debug.Log("set: from " + onChange?.GetInvocationList().Length + " to " + value?.GetInvocationList().Length);
    //         onChange = value;
    //     }
    // }

    private Card[] cards;
    public Card[] Cards => cards;
    

    public Inventory(uint size)
    {
        cards = new Card[size];
    }

    public Inventory(Card[] cards)
    {
        this.cards = (Card[])cards.Clone();
    }

    public void Set(Card[] cards)
    {
        if (cards.Length != this.cards.Length)
        { throw new ArgumentException(); }

        this.cards = cards;
        OnChange.Invoke();
    }

    public static Inventory operator +(Inventory a, Inventory b)
    {
        List<Card> cards = new List<Card>();
        cards.AddRange(a.cards);
        cards.AddRange(b.cards);
        cards.RemoveAll(card => { return card == null; });
        Inventory result = new Inventory(cards.ToArray());
        return result;
    }

    public static void Swap(Slot slot1, Slot slot2)
    {
        Card temp = slot1.inventory.cards[slot1.index];
        slot1.inventory.cards[slot1.index] = slot2.inventory.cards[slot2.index];
        slot2.inventory.cards[slot2.index] = temp;
        slot1.inventory.OnChange?.Invoke();
        slot2.inventory.OnChange?.Invoke();
    }

    public bool TakeFrom(Computer computer, int targetSlot = -1)
    {
        if (targetSlot == -1)
        {
            targetSlot = GetFreeSlot();
            if (targetSlot == -1)
            {
                return false;
            }
        }
        Card card = computer.TakeCard();
        if (card == null)
        {
            return false;
        }

        cards[targetSlot] = card;
        OnChange?.Invoke();
        return true;
    }

    private int FindCard(Card card)
    {
        int cardSlot = -1;
        for (int i = 0; i < cards.Length; i++)
        {
            if (cards[i] == card)
            {
                cardSlot = i;
                break;
            }
        }

        return cardSlot;
    }

    public int GetFreeSlot()
    {
        return FindCard(null);
    }

    private bool AddCard(Card card)
    {
        int freeSlot = GetFreeSlot();
        if (freeSlot == -1)
        {
            return false;
        }

        cards[freeSlot] = card;
        OnChange?.Invoke();
        return true;
    }

    public bool RemoveCard(Card card)
    {
        int slot = FindCard(card);
        if (slot == -1 || card == null)
        {
            return false;
        }

        cards[slot] = null;
        return true;
    }

    public void RemoveCard(uint slot)
    {
        cards[slot] = null;
        OnChange?.Invoke();
    }

    public bool Steal(Slot from, Computer atComputer)
    {
        if (!atComputer.CanStealFrom(from.inventory) || from.inventory.cards[from.index] == null)
        {
            return false;
        }

        bool success = AddCard(from.inventory.cards[from.index]);
        if (!success)
        {
            return false;
        }

        from.inventory.RemoveCard(from.index);
        return true;
    }
}
